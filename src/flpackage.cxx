// generated by Fast Light User Interface Designer (fluid) version 1.0304

#include "flpackage.h"
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>
#include <unistd.h>
#include <dirent.h>
#include <limits.h>

int fexist( const char *a_option ) {
  char dir1[PATH_MAX]; 
  	char *dir2;
  	DIR *dip;
  	strncpy( dir1 , "",  PATH_MAX  );
  	strncpy( dir1 , a_option,  PATH_MAX  );
  
  	struct stat st_buf; 
  	int status; 
  	int fileordir = 0 ; 
  
  	status = stat ( dir1 , &st_buf);
  	if (status != 0) {
  		fileordir = 0;
  	}
  	FILE *fp2check = fopen( dir1  ,"r");
  	if( fp2check ) {
  		fileordir = 1; 
  		fclose(fp2check);
  	} 
  
  	if (S_ISDIR (st_buf.st_mode)) {
  		fileordir = 2; 
  	}
  	return fileordir;
  	/////////////////////////////
}

Fl_Double_Window *win1=(Fl_Double_Window *)0;

static void cb_Quit(Fl_Button*, void*) {
  printf( " Bye ! \n" );


exit( 0 );
}

static void cb_Run(Fl_Button*, void*) {
  result_browser->clear();
 result_browser->add( "text" );
 statusbar->value( inputfield->value()  );
 
 result_browser->clear();
 
 
 
 	char cmdi[PATH_MAX];
 
   
   	strncpy( cmdi , " "  , PATH_MAX );
	strncat( cmdi , " " , PATH_MAX - strlen( cmdi ) -1 );
	strncat( cmdi , " apt-cache search   "  , PATH_MAX - strlen( cmdi ) -1 );
	strncat( cmdi , inputfield->value()  , PATH_MAX - strlen( cmdi ) -1 );
	strncat( cmdi , " >   "  , PATH_MAX - strlen( cmdi ) -1 );
	strncat( cmdi , "  /tmp/datatillo.dat   " , PATH_MAX - strlen( cmdi ) -1 );
	system( cmdi ); 
   
   
   
   
  
  printf("Load Panel!\n");
  
  int fetchi;
  FILE *fp5;
  FILE *fp6;
  char fetchline[PATH_MAX];
  char fetchlinetmp[PATH_MAX];

  result_browser->clear( );    
    
  if ( fexist( "/tmp/datatillo.dat" ) == 1 ) 
  {
    fp6 = fopen( "/tmp/datatillo.dat" , "rb");
    while( !feof(fp6) ) 
    {
          fgets(fetchlinetmp, PATH_MAX, fp6); 
          strncpy( fetchline, "" , PATH_MAX );
          for( fetchi = 0 ; ( fetchi <= strlen( fetchlinetmp ) ); fetchi++ )
            if ( fetchlinetmp[ fetchi ] != '\n' )
                 fetchline[fetchi]=fetchlinetmp[fetchi];
                 
                if ( !feof( fp6 ) ) 
                {
                    result_browser->add( fetchline );    
                }

     }
     fclose( fp6 );
 };
}

Fl_Browser *result_browser=(Fl_Browser *)0;

Fl_Input *inputfield=(Fl_Input *)0;

Fl_Output *statusbar=(Fl_Output *)0;

Fl_Double_Window* make_window() {
  { win1 = new Fl_Double_Window(650, 380, "Screen");
    { Fl_Button* o = new Fl_Button(510, 35, 135, 25, "&Quit");
      o->labelfont(1);
      o->callback((Fl_Callback*)cb_Quit);
    } // Fl_Button* o
    { Fl_Button* o = new Fl_Button(340, 35, 160, 25, "&Run Search");
      o->labelfont(1);
      o->callback((Fl_Callback*)cb_Run);
    } // Fl_Button* o
    { Fl_Group* o = new Fl_Group(5, 75, 640, 270);
      o->box(FL_DOWN_BOX);
      { result_browser = new Fl_Browser(10, 80, 630, 260);
        result_browser->type( FL_HOLD_BROWSER  );
      } // Fl_Browser* result_browser
      o->end();
      Fl_Group::current()->resizable(o);
    } // Fl_Group* o
    { Fl_Group* o = new Fl_Group(5, 5, 640, 20, "FLPACKAGE -- The Little FLTK");
      o->box(FL_UP_BOX);
      o->labeltype(FL_ENGRAVED_LABEL);
      o->align(Fl_Align(FL_ALIGN_CENTER));
      o->end();
    } // Fl_Group* o
    { inputfield = new Fl_Input(10, 35, 325, 25);
      inputfield->value( "www.netbsd.org" );
    } // Fl_Input* inputfield
    { statusbar = new Fl_Output(5, 350, 640, 25);
      statusbar->color(FL_BACKGROUND_COLOR);
    } // Fl_Output* statusbar
    win1->end();
  } // Fl_Double_Window* win1
  return win1;
}

int main( int argc, char *argv[]) {
  printf( " == FLTK == \n" );
  
  ////////////////////////////////////////////////////////
    if ( argc == 2)
    if ( strcmp( argv[1] , "" ) !=  0 )
    {
            chdir( argv[ 1 ] );
    }
  
    char mydirnow[2500];
    printf( "Current Directory: %s \n", getcwd( mydirnow, 2500 ) );
  
  
    make_window();
    win1->show();
  
    Fl::run();
}
